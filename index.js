import express from 'express';
import cors from 'cors'
import $ from "jquery";
//import a from './views/urlScraper.js'
import fetch from 'node-fetch';
import {
    load
} from 'cheerio';
const PORT = 5000;

const app = express();
app.use(cors())

app.set('view engine', 'ejs');
app.use(express.urlencoded({
    extended: true
}))



app.get('/', (req, res) => {
    res.render('pages/index', {
        gData:"",
        url:"",
        warning:""
    });
})

app.post('/abc', (req, res) => {
    let data = req.body.urlName
    async function callAll(a) {
        const url = `${a}`;
        //console.log(b)
        const response = await fetch(url);
        const body = await response.text();
        let allData= []

        let $ = load(body);
        let tag=['h1','h2','h3','h4','h5','h6']

        for(let i=0;i<tag.length;i++){
            let b = $(tag[i])
            b.each((i,e) => {
               allData.push($(e).text())

            });
        }
        //console.log(z)

        

       res.render('pages/index',{
        gData:allData,
        url: url,
        warning:""
       })

    }

    if(data!=""){
            callAll(data)
        }
        else{
        res.render('pages/index',{
            gData:"",
            url:"",
            warning:"Please Provide URL"

           })
    
    }
     

    //console.log(z)


})




app.listen(PORT, () => {
    console.log(`Running on PORT ${PORT}`);
})